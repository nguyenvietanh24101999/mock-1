package com.mock.mockproject1.service;

import com.mock.mockproject1.dto.TicketDTO;
import com.mock.mockproject1.mapper.TicketMapper;
import com.mock.mockproject1.model.Ticket;
import com.mock.mockproject1.repository.TicketRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TicketService {
    private final TicketRepository ticketRepository;
    private final TicketMapper ticketMapper;

    public TicketService(TicketRepository ticketRepository, TicketMapper ticketMapper) {
        this.ticketRepository = ticketRepository;
        this.ticketMapper = ticketMapper;
    }

    public List<TicketDTO> getAllTicket() {
        return ticketMapper.mapTicketToTicketDTO(ticketRepository.findAll());
    }

    public Ticket saveTicket(Ticket ticket) {
        return ticketRepository.save(ticket);
    }

    public void deleteTicket(Long id) {
        ticketRepository.deleteById(id);
    }

    public Ticket updateTicket(Long id, Ticket ticket) {
        Optional<Ticket> opt = ticketRepository.findById(id);
        if (opt.isPresent()) {
            Ticket ticketUpdate = opt.get();
            ticketUpdate.setTicketId(ticket.getTicketId());
            ticketUpdate.setBookingTime(ticket.getBookingTime());
            ticketUpdate.setCustomerName(ticket.getCustomerName());
            ticketUpdate.setTrip(ticket.getTrip());
            ticketUpdate.setCar(ticket.getCar());

            return ticketRepository.save(ticketUpdate);
        } else {
            return null;
        }
    }
}
