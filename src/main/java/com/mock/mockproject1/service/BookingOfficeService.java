package com.mock.mockproject1.service;

import com.mock.mockproject1.dto.BookingOfficeDTO;
import com.mock.mockproject1.mapper.BookingOfficeMapper;
import com.mock.mockproject1.model.BookingOffice;
import com.mock.mockproject1.repository.BookingOfficeRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BookingOfficeService {
    private final BookingOfficeRepository bookingOfficeRepository;
    private final BookingOfficeMapper bookingOfficeMapper;

    public BookingOfficeService(BookingOfficeRepository bookingOfficeRepository, BookingOfficeMapper bookingOfficeMapper) {
        this.bookingOfficeRepository = bookingOfficeRepository;
        this.bookingOfficeMapper = bookingOfficeMapper;
    }

    public List<BookingOfficeDTO> getAllBookingOffice() {
        return bookingOfficeMapper.mapperOfficeToOfficeDTO(bookingOfficeRepository.findAll());
    }

    public BookingOffice saveBookingOffice(BookingOffice bookingOffice) {
        return bookingOfficeRepository.save(bookingOffice);
    }

    public void deleteBookingOffice(Long id) {
        bookingOfficeRepository.deleteById(id);
    }

    public BookingOffice updateBookingOffice(Long id, BookingOffice bookingOffice) {
        Optional<BookingOffice> opt = bookingOfficeRepository.findById(id);
        if (opt.isPresent()) {
            BookingOffice newBookingOffice = opt.get();
            newBookingOffice.setOfficeId(bookingOffice.getOfficeId());
            newBookingOffice.setOfficePrice(bookingOffice.getOfficePrice());
            newBookingOffice.setOfficePhone(bookingOffice.getOfficePhone());
            newBookingOffice.setOfficePlace(bookingOffice.getOfficePlace());
            newBookingOffice.setOfficeName(bookingOffice.getOfficeName());
            newBookingOffice.setStartContractDeadline(bookingOffice.getStartContractDeadline());
            newBookingOffice.setEndContractDeadline(bookingOffice.getEndContractDeadline());

            return bookingOfficeRepository.save(newBookingOffice);
        } else {
            return null;
        }
    }
}
