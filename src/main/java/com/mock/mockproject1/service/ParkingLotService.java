package com.mock.mockproject1.service;

import com.mock.mockproject1.dto.ParkingLotDTO;
import com.mock.mockproject1.mapper.ParkingLotMapper;
import com.mock.mockproject1.model.ParkingLot;
import com.mock.mockproject1.repository.ParkingLotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ParkingLotService {
    private final ParkingLotRepository parkingLotRepository;
    private final ParkingLotMapper parkingLotMapper;

    public ParkingLotService(ParkingLotRepository parkingLotRepository, ParkingLotMapper parkingLotMapper) {
        this.parkingLotRepository = parkingLotRepository;
        this.parkingLotMapper = parkingLotMapper;
    }

    public List<ParkingLotDTO> getAllParkingLot() {
        return parkingLotMapper.mapperParkingLotToParkingLotDTO(parkingLotRepository.findAll());
    }

    public ParkingLot saveParkingLot(ParkingLot parkingLot) {
        return parkingLotRepository.save(parkingLot);
    }

    public void deleteParkingLot(Long id) {
        parkingLotRepository.deleteById(id);
    }

    public ParkingLot updateParkingLot(Long id, ParkingLot parkingLot) {
        Optional<ParkingLot> opt = parkingLotRepository.findById(id);
        if (opt.isPresent()) {
            ParkingLot pkUpdate = opt.get();
            pkUpdate.setParkId(parkingLot.getParkId());
            pkUpdate.setParkPrice(parkingLot.getParkPrice());
            pkUpdate.setParkStatus(parkingLot.getParkStatus());
            pkUpdate.setParkPlace(parkingLot.getParkPlace());
            pkUpdate.setParkArea(parkingLot.getParkArea());
            pkUpdate.setParkName(parkingLot.getParkName());

            return parkingLotRepository.save(pkUpdate);
        } else {
            return null;
        }
    }
}
