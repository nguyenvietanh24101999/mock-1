package com.mock.mockproject1.service;

import com.mock.mockproject1.dto.EmployeeDTO;
import com.mock.mockproject1.mapper.EmployeeMapper;
import com.mock.mockproject1.model.Employee;
import com.mock.mockproject1.repository.EmployeeRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {
    private final EmployeeRepository employeeRepository;
    private final EmployeeMapper employeeMapper;

    public EmployeeService(EmployeeRepository employeeRepository, EmployeeMapper employeeMapper) {
        this.employeeRepository = employeeRepository;
        this.employeeMapper = employeeMapper;
    }

    public Employee saveEmployee(Employee employee) {
        return employeeRepository.save(employee);
    }

    public List<EmployeeDTO> getListEmployee() {
        return employeeMapper.mapperEmployeeToEmployeeDTO(employeeRepository.findAll());
    }

    public void deleteEmployee(Long id) {
        employeeRepository.deleteById(id);
    }

    public Employee updateEmployee(Long id, Employee employee) {
        Optional<Employee> opt = employeeRepository.findById(id);
        if (opt.isPresent()) {
            Employee employeeUpdate = opt.get();
            employeeUpdate.setEmployeeId(employee.getEmployeeId());
            employeeUpdate.setEmployeeName(employee.getEmployeeName());
            employeeUpdate.setEmployeePhone(employee.getEmployeePhone());
            employeeUpdate.setEmployeeEmail(employee.getEmployeeEmail());
            employeeUpdate.setEmployeeAddress(employee.getEmployeeAddress());
            employeeUpdate.setEmployeeBirthday(employee.getEmployeeBirthday());
            employeeUpdate.setGender(employee.getGender());
            employeeUpdate.setPassword(employee.getPassword());
            employeeUpdate.setAccount(employee.getAccount());
            employeeUpdate.setDepartment(employee.getDepartment());

            return employeeRepository.save(employeeUpdate);
        } else {
            return null;
        }
    }
}
