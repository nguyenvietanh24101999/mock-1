package com.mock.mockproject1.service;

import com.mock.mockproject1.dto.TripDTO;
import com.mock.mockproject1.mapper.TripMapper;
import com.mock.mockproject1.model.Trip;
import com.mock.mockproject1.repository.TripRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TripService {
    private final TripRepository tripRepository;
    private final TripMapper tripMapper;

    public TripService(TripRepository tripRepository, TripMapper tripMapper) {
        this.tripRepository = tripRepository;
        this.tripMapper = tripMapper;
    }

    public List<TripDTO> getAllTrip() {
        return tripMapper.mapperTripToTripDTO(tripRepository.findAll());
    }

    public Trip saveTrip(Trip trip) {
        return tripRepository.save(trip);
    }

    public void deleteTrip(Long id) {
        tripRepository.deleteById(id);
    }

    public Trip updateTrip(Long id, Trip trip) {
        Optional<Trip> opt = tripRepository.findById(id);
        if (opt.isPresent()) {
            Trip tripUpdate = opt.get();
            tripUpdate.setTripId(trip.getTripId());
            tripUpdate.setDriver(trip.getDriver());
            tripUpdate.setDestination(trip.getDestination());
            tripUpdate.setDepartureTime(trip.getDepartureTime());
            tripUpdate.setDepartureDate(trip.getDepartureDate());
            tripUpdate.setCarType(trip.getCarType());
            tripUpdate.setMaximumOnlineTicketNumber(trip.getMaximumOnlineTicketNumber());
            tripUpdate.setBookedTicketNumber(trip.getBookedTicketNumber());

            return tripRepository.save(tripUpdate);
        } else {
            return null;
        }
    }
}
