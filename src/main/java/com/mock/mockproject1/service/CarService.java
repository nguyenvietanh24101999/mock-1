package com.mock.mockproject1.service;

import com.mock.mockproject1.dto.CarDTO;
import com.mock.mockproject1.mapper.CarMapper;
import com.mock.mockproject1.model.Car;
import com.mock.mockproject1.repository.CarRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CarService {
    private final CarRepository carRepository;
    private final CarMapper carMapper;

    public CarService(CarRepository carRepository, CarMapper carMapper) {
        this.carRepository = carRepository;
        this.carMapper = carMapper;
    }

    public List<CarDTO> getAllCar() {
        return carMapper.mapperCarToCarDTO(carRepository.findAll());
    }

    public Car saveCar(Car car) {
        return carRepository.save(car);
    }

    public void deleteCar(String id) {
        carRepository.deleteById(id);
    }

    public Car updateCar(String id, Car car) {
        Optional<Car> opt = carRepository.findById(id);
        if (opt.isPresent()) {
            Car carUpdate = opt.get();
            carUpdate.setLicensePlate(car.getLicensePlate());
            carUpdate.setCarType(car.getCarType());
            carUpdate.setCarColor(car.getCarColor());
            carUpdate.setCompany(car.getCompany());
            carUpdate.setParkingLot(car.getParkingLot());

            return carRepository.save(carUpdate);
        } else {
            return null;
        }
    }
}
