package com.mock.mockproject1.dto;

import lombok.Data;

import java.sql.Time;

@Data
public class TicketDTO {
    private Long ticketId;
    private String trip;
    private String licensePlate;
    private String customerName;
    private Time bookingTime;
}
