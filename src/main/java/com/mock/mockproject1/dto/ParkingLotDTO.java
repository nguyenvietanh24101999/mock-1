package com.mock.mockproject1.dto;

import lombok.Data;

@Data
public class ParkingLotDTO {
    private Long parkId;
    private Long parkArea;
    private String parkName;
    private String parkPlace;
    private Long parkPrice;
    private String parkStatus;
}
