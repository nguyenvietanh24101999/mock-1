package com.mock.mockproject1.dto;

import lombok.Data;

import java.sql.Date;

@Data
public class EmployeeDTO {
    private Long employeeId;
    private String department;
    private String employeeAddress;
    private Date employeeBirthday;
    private String employeeName;
    private String employeePhone;
}
