package com.mock.mockproject1.dto;

import lombok.Data;

@Data
public class BookingOfficeDTO {
    private Long officeId;
    private String officeName;
    private String trip;
}
