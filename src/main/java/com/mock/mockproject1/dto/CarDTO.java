package com.mock.mockproject1.dto;

import lombok.Data;

@Data
public class CarDTO {
    private String licensePlate;

    private String carColor;
    private String carType;
    private String company;
    private String parkingLot;
}
