package com.mock.mockproject1.dto;

import lombok.Data;

import java.sql.Time;

@Data
public class TripDTO {
    private Long tripId;
    private int bookedTicketNumber;
    private String carType;
    private Time departureTime;
    private String destination;
    private String driver;
}
