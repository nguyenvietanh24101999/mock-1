package com.mock.mockproject1.controller;

import com.mock.mockproject1.dto.BookingOfficeDTO;
import com.mock.mockproject1.model.BookingOffice;
import com.mock.mockproject1.service.BookingOfficeService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v2/office")
public class BookingOfficeController {
    private final BookingOfficeService officeService;

    public BookingOfficeController(BookingOfficeService officeService) {
        this.officeService = officeService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<BookingOfficeDTO>> getAllOffice() {
        try {
            return new ResponseEntity<>(officeService.getAllBookingOffice(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/save")
    public ResponseEntity<BookingOffice> saveOffice(@RequestBody BookingOffice bookingOffice) {
        try {
            return new ResponseEntity<>(officeService.saveBookingOffice(bookingOffice), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<BookingOffice> deleteBookingOffice(@PathVariable(name = "id") Long id) {
        try {
            officeService.deleteBookingOffice(id);
            return new ResponseEntity<>(null, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<BookingOffice> updateBookingOffice(@PathVariable(name = "id") Long id,
                                                             @RequestBody BookingOffice bookingOffice) {
        try {
            return new ResponseEntity<>(officeService.updateBookingOffice(id, bookingOffice), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
