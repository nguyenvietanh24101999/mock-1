package com.mock.mockproject1.controller;

import com.mock.mockproject1.dto.TripDTO;
import com.mock.mockproject1.model.Trip;
import com.mock.mockproject1.service.TripService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v2/trip")
public class TripController {
    private final TripService tripService;

    public TripController(TripService tripService) {
        this.tripService = tripService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<TripDTO>> getAllTrip() {
        try {
            return new ResponseEntity<>(tripService.getAllTrip(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/save")
    public ResponseEntity<Trip> saveTrip(@RequestBody Trip trip) {
        try {
            return new ResponseEntity<>(tripService.saveTrip(trip), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Trip> deleteTrip(@PathVariable(name = "id") Long id) {
        try {
            tripService.deleteTrip(id);
            return new ResponseEntity<>(null, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Trip> updateTrip(@PathVariable(name = "id") Long id,
                                           @RequestBody Trip trip) {
        try {
            return new ResponseEntity<>(tripService.updateTrip(id, trip), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
