package com.mock.mockproject1.controller;

import com.mock.mockproject1.dto.ParkingLotDTO;
import com.mock.mockproject1.model.ParkingLot;
import com.mock.mockproject1.service.ParkingLotService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v2/parkingLot")
public class ParkingLotController {
    private final ParkingLotService parkingLotService;

    public ParkingLotController(ParkingLotService parkingLotService) {
        this.parkingLotService = parkingLotService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<ParkingLotDTO>> getAllLot() {
        try {
            return new ResponseEntity<>(parkingLotService.getAllParkingLot(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/save")
    public ResponseEntity<ParkingLot> saveParkingLot(@RequestBody ParkingLot parkingLot) {
        try {
            return new ResponseEntity<>(parkingLotService.saveParkingLot(parkingLot), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<ParkingLot> deleteParkingLot(@PathVariable(name = "id") Long id) {
        try {
            parkingLotService.deleteParkingLot(id);
            return new ResponseEntity<>(null, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<ParkingLot> updateParkingLot(@PathVariable(name = "id") Long id,
                                                       @RequestBody ParkingLot parkingLot) {
        try {
            return new ResponseEntity<>(parkingLotService.updateParkingLot(id, parkingLot), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
