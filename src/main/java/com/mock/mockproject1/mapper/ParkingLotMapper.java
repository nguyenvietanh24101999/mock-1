package com.mock.mockproject1.mapper;

import com.mock.mockproject1.dto.ParkingLotDTO;
import com.mock.mockproject1.model.ParkingLot;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ParkingLotMapper {
    private final ModelMapper modelMapper;

    public ParkingLotMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public List<ParkingLotDTO> mapperParkingLotToParkingLotDTO(List<ParkingLot> parkingLots) {
        return parkingLots.stream()
                .map(parkingLot -> modelMapper.map(parkingLot, ParkingLotDTO.class))
                .collect(Collectors.toList());
    }
}
