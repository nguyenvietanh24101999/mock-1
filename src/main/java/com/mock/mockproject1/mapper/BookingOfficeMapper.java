package com.mock.mockproject1.mapper;

import com.mock.mockproject1.dto.BookingOfficeDTO;
import com.mock.mockproject1.model.BookingOffice;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class BookingOfficeMapper {
    private final ModelMapper modelMapper;

    public BookingOfficeMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public List<BookingOfficeDTO> mapperOfficeToOfficeDTO(List<BookingOffice> bookingOffices) {
        return bookingOffices.stream()
                .map(bookingOffice -> modelMapper.map(bookingOffice, BookingOfficeDTO.class))
                .collect(Collectors.toList());
    }
}
