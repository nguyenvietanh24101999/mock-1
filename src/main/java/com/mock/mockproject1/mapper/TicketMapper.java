package com.mock.mockproject1.mapper;

import com.mock.mockproject1.dto.TicketDTO;
import com.mock.mockproject1.model.Ticket;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class TicketMapper {
    public List<TicketDTO> mapTicketToTicketDTO(List<Ticket> tickets) {
        ModelMapper mapper = new ModelMapper();
        TypeMap<Ticket, TicketDTO> propertyMapper = mapper.createTypeMap(Ticket.class, TicketDTO.class);
        propertyMapper.addMappings(
                myMapper ->   myMapper.map(src -> src.getCar().getLicensePlate(), TicketDTO::setLicensePlate)
        );
        propertyMapper.addMappings(
                myMapper ->   myMapper.map(src -> src.getTrip().getDestination(), TicketDTO::setTrip)
        );

        List<TicketDTO> listTicketDTO = new ArrayList<>();
        for (Ticket ticket: tickets) {
            listTicketDTO.add(mapper.map(ticket, TicketDTO.class));
        }

        return listTicketDTO;
    }
}
