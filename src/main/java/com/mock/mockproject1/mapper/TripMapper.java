package com.mock.mockproject1.mapper;

import com.mock.mockproject1.dto.TripDTO;
import com.mock.mockproject1.model.Trip;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class TripMapper {
    private final ModelMapper modelMapper;

    public TripMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public List<TripDTO> mapperTripToTripDTO(List<Trip> trips) {
        return trips.stream().map(trip -> modelMapper.map(trip, TripDTO.class)).collect(Collectors.toList());
    }
}
