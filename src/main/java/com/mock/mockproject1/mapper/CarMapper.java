package com.mock.mockproject1.mapper;

import com.mock.mockproject1.dto.CarDTO;
import com.mock.mockproject1.model.Car;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CarMapper {
    private final ModelMapper modelMapper;

    public CarMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public List<CarDTO> mapperCarToCarDTO(List<Car> cars) {
        TypeMap<Car, CarDTO> propertyMapper = modelMapper.createTypeMap(Car.class, CarDTO.class);
        propertyMapper.addMappings(myMapper -> myMapper.map(src -> src.getParkingLot().getParkName(), CarDTO::setParkingLot));

        return cars.stream().map(car -> modelMapper.map(car, CarDTO.class)).collect(Collectors.toList());
    }
}
