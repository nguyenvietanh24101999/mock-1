package com.mock.mockproject1.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Time;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Ticket {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ticketId;
    private Time bookingTime;

    @Column(nullable = false)
    private String customerName;

    @ManyToOne
    @JoinColumn(name = "licensePlate")
    private Car car;

    @ManyToOne
    @JoinColumn(name = "tripId")
    private Trip trip;
}
