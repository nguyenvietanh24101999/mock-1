package com.mock.mockproject1.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Car {
    @Id
    private String licensePlate;

    private String carColor;
    private String carType;
    private String company;

    @ManyToOne
    @JoinColumn(name = "parkId")
    private ParkingLot parkingLot;

}
